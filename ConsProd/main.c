/*
 * Author:     Kaleb Eberhart
 * Date:       09/17/2017
 * Course:     CST-221
 * Assignment: Producer and Consumer
 */

#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>

//A pre-defined buffer limit, change as required
#define BufferLimit 10
 
//Initializes the buffer index value and declares queue
int bufferIndexValue = 0;
char *bufferQueue;
 
//Pthread initializers
pthread_mutex_t mutVar = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t bufferQueNotFull = PTHREAD_COND_INITIALIZER;
pthread_cond_t bufferQueNotEmpty = PTHREAD_COND_INITIALIZER;
 
//This method will either consume or wait if the buffer is empty
void *Consumer()
{
      while(1)
      {
            pthread_mutex_lock(&mutVar);
            if(bufferIndexValue == -1)
            {            
                  pthread_cond_wait(&bufferQueNotEmpty, &mutVar);
            }                
            printf("Consumer:%d\t", bufferIndexValue--);        
            pthread_mutex_unlock(&mutVar);        
            pthread_cond_signal(&bufferQueNotFull);                
      }    
}
 
//This method will continue producing unless the Buffer is full
void *Producer()
{    
      while(1)
      { 
            pthread_mutex_lock(&mutVar);
            if(bufferIndexValue == BufferLimit)
            {                        
                  pthread_cond_wait(&bufferQueNotFull, &mutVar);
            }                        
            bufferQueue[bufferIndexValue++] = '@';
            printf("Producer:%d\t", bufferIndexValue);
            pthread_mutex_unlock(&mutVar);
            pthread_cond_signal(&bufferQueNotEmpty);        
      }    
}
 
//This method runs producer and consumer
int main()
{    
      pthread_t prodThreadID, consThreadID;
      bufferQueue = (char *) malloc(sizeof(char) * BufferLimit);            
      pthread_create(&prodThreadID, NULL, Producer, NULL);
      pthread_create(&consThreadID, NULL, Consumer, NULL);
      pthread_join(prodThreadID, NULL);
      pthread_join(consThreadID, NULL);
      return 0;
}